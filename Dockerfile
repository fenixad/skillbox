FROM node

RUN apt-get update && apt-get install -y nano vim

RUN mkdir /tetris
COPY package.json /tetris
WORKDIR /tetris

RUN yarn install
COPY . /tetris
RUN yarn test && yarn build
CMD yarn start

EXPOSE 3000
